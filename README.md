**Cache Utility** module provides an ability to view status and flush various caches in following three ways:

*   Web browser user interface
*   Curl commands
*   Drush commands

The following caches are supported:

*   PHP OPCache
*   PHP APCu extension cache
*   Database cache\_\* tables
*   Database cachetags table
*   Clear Drupal cache via Curl

### Other features

*   The module may also be configured to flush any of the above caches along with Drupal’s cache flush.
*   An optional sub-module adds links for flushing above caches under the "Drupal icon > Flush all Caches" via the contributed [Admin Toolbar Extras](https://www.drupal.org/project/admin_toolbar "Admin Toolbar Extras") module.

### Benefits

1.  The Curl commands provide convenience of flushing various caches externally from the webserver. A single long secret code may be set to manage cache flushing on multiple sites.
2.  PHP OPCache, and APCu caches can be flushed without server restarts or adding obfuscated php files to the webserver.
3.  Enables flushing of PHP OPCache via drush which is normally not possible as drush executes using PHP-CLI and not the PHP used by webserver.
4.  Provides an ability to truncate cachetags database table which is not managed by Drupal core and in certain cases can have unlimited growth affecting database size. [See issue](https://www.drupal.org/project/drupal/issues/3097393 "See issue")
5.  Provides an ability to truncate cache\_\* database tables

### Curl and Drush command documentation

Documentation for the curl and drush command for each cache type is included on the module's configuration page at Configuration > Development > Cache Utility  
Or at this path /admin/config/development/cache\_utility

### Known issue

Truncating cache\_\* table for sqlite database does not work, due to Drupal core [issue#2949229](https://www.drupal.org/project/drupal/issues/2949229)
