<?php

namespace Drupal\cache_utility_admin_toolbar\Controller;

use Drupal\cache_utility\Controller\CU_Cachetags;
use Drupal\cache_utility\Controller\CU_DrupalCache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\cache_utility\Controller\CU_APCu_Clear;
use Drupal\cache_utility\Controller\CU_OPCache_Clear;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CU_FlushCaches extends ControllerBase
{
    /**
     * Reload the previous page.
     */
    public function reloadPage() {
        $request = \Drupal::request();
        if ($request->server->get('HTTP_REFERER')) {
            return $request->server->get('HTTP_REFERER');
        }
        else {
            return '/';
        }
    }

    /**
     * Flush OP Cache
     * @return RedirectResponse
     */
    public function flushOPCache() {
        if (CU_OPCache_Clear::isOPCacheEnabled()) {
            CU_OPCache_Clear::resetOPCache();
            $this->messenger()->addMessage($this->t('OPCache flushed.'));
        }
        return new RedirectResponse($this->reloadPage());
    }

    /**
     * Flush APCu Cache
     * @return RedirectResponse
     */
    public function flushAPCuCache() {
        if (CU_APCu_Clear::isAPCuEnabled()) {
            CU_APCu_Clear::clearAPCuCache();
            $this->messenger()->addMessage($this->t('APCu flushed.'));
        }
        return new RedirectResponse($this->reloadPage());
    }

    /**
     * Truncate the cachetags table
     * @return RedirectResponse
     */
    public function flushDrupalCachetags() {
        if (CU_Cachetags::doesCachetagsTableExist()) {
            CU_Cachetags::clearCachetagsTable();
            $this->messenger()->addMessage($this->t("Database's 'cachetags' table flushed."));
        }
        return new RedirectResponse($this->reloadPage());
    }

    /**
     * Truncate all cache_* database tables
     * @return RedirectResponse
     */
    public function flushDrupalCachetables() {
        $numrows = CU_DrupalCache::getNumRowsInAllCacheTables();
        CU_DrupalCache::truncateAllDrupalCacheTables();
        $this->messenger()->addMessage($this->t("Cleared $numrows rows from all 'cache_*' tables."));
        return new RedirectResponse($this->reloadPage());
    }
}
