<?php

namespace Drupal\cache_utility\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal;
use Drupal\Core\Url;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class CuCommands extends DrushCommands
{

    /**
     * View the status details of OP Cache.
     *
     * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
     *
     * @field-labels
     *   group: Group
     *   property: Property
     *   value: Value
     * @default-fields group,property,value
     *
     * @filter-default-field group
     * @usage cache_utility:opcache-status
     * @command cache_utility:opcache-status
     * @aliases cache_utility:opcache,cu:opcache,cu:opcache-status
     * @return RowsOfFields
     */
    public function opcache_status($options = ['host' => NULL])
    {
        $host = $this->getHost($options);
        if (!$host) {
            return 1;
        }

        $config = Drupal::config('cache_utility.settings');
        $accessKey = $config->get("security.accessKey");

        $resp = $this->makeJSONRequest($host, "cache_utility.opcache.status", $accessKey);
        if (!$resp) {
            return 1;
        }

        foreach ($resp['opcache_status'] as $key => $val) {
            if ($key == "scripts") {
                continue;
            }
            if (in_array($key, ['opcache_enabled', 'cache_full', 'restart_pending', 'restart_in_progress'])) {
                $rows[] = [
                    'group' => '',
                    'property' => $key,
                    'value' => $val
                ];
            } else {
                foreach ($val as $sub_key => $sub_val) {
                    $rows[] = [
                        'group' => $key,
                        'property' => $sub_key,
                        'value' => $sub_val
                    ];
                }
            }
        }
        return new RowsOfFields($rows);
    }

    /**
     * Get the host from the input options.
     *
     * @param $options
     * @return string|null
     */
    private function getHost($options) {
        $host = isset($options['host']) ? $options['host'] : NULL;
        if (!$host) {
            // If no host parameter was provided, try to auto-locate one.
            global $base_url;
            $host = $base_url;

            // ignore default base_url values, which are invalid
            if ("http://default" == $host) {
                $host = NULL;
            }

            if (!$host) {
                // If no valid host could be auto-located (e.g., base_url was not set, etc.), default to http://localhost
                $this->logger()->warning(dt("No 'host' was provided nor auto-located, so defaulting to: http://localhost."));
                $host = "http://localhost";
            }
        } else if (is_string($host)) {
            // A (string) host parameter was provided
            $host = $this->validateHost($host);

            // If the provided host is invalid, throw an error
            if (!$host) {
                $this->logger()->error(dt("Invalid 'host' provided."));
                return NULL;
            }
        }
        return $host;
    }

    /**
     * Make a request to the input route to get back a JSON response.
     *
     * @param $host
     * @param $route_name
     * @param $accessKey
     * @return mixed|null
     */
    private function makeJSONRequest($host, $route_name, $accessKey)
    {
        if ($host && is_string($host)) {
            $endpoint_url_path = Url::fromRoute($route_name)->toString();
            $endpoint_url = "$host$endpoint_url_path";
        } else {
            $endpoint_url = Url::fromRoute($route_name)->setAbsolute(TRUE);
            $endpoint_url = $endpoint_url->toString();
        }

        $ch = curl_init();

        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Set the url
        curl_setopt($ch, CURLOPT_URL, $endpoint_url);

        // Add headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "CU-ACCESS-KEY: $accessKey",
        ));

        // If SSL verification should be skipped on request set CURL params
        // to skip verifications.
        $config = Drupal::config('cache_utility.settings');
        $skip_ssl_verification = (bool) $config->get('skip_ssl_verification');
        if ($skip_ssl_verification === TRUE) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

        // Execute
        $result = curl_exec($ch);

        // Closing
        curl_close($ch);

        $resp = json_decode($result, true);
        if (!$resp) {
            $this->logger()->error(dt("Please provide a valid 'host' parameter."));
            return NULL;
        }

        if (!$resp['success']) {
            if (array_key_exists("error", $resp)) {
                $this->logger()->error(dt($resp['error']));
            } else {
                $this->logger()->error(dt("Oops, an error occurred."));
            }
            return NULL;
        }

        return $resp;
    }

    /**
     * Validate that the input host is valid.
     *
     * @param $host
     * @return string|null
     */
    private function validateHost($host)
    {
        // Source: https://www.w3schools.in/php-script/check-domain-name-is-valid/
        $validation = FALSE;
        $url = NULL;

        /*Parse URL*/
        $urlparts = parse_url(filter_var($host, FILTER_SANITIZE_URL));

        /*Check host exist else path assign to host*/
        if (!isset($urlparts['host'])) {
            $urlparts['host'] = $urlparts['path'];
        }

        if ($urlparts['host'] != '') {

            /*Add scheme if not found*/
            if (!isset($urlparts['scheme'])) {
                $urlparts['scheme'] = 'http';
            }

            /*Validation*/
            if (checkdnsrr($urlparts['host'], 'A')
                && in_array($urlparts['scheme'], array('http', 'https'))
                && ip2long($urlparts['host']) === FALSE) {

                $urlparts['host'] = preg_replace('/^www\./', '', $urlparts['host']);
                $url = $urlparts['scheme'] . '://' . $urlparts['host'];

                if (filter_var($url, FILTER_VALIDATE_URL) !== false && @get_headers($url)) {
                    $validation = TRUE;
                }
            }
        }
        return $url;
    }

    /**
     * View the status details of APCu.
     *
     * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
     *
     * @field-labels
     *   property: Property
     *   value: Value
     * @default-fields property,value
     *
     * @filter-default-field property
     * @usage cache_utility:apcu-status
     * @command cache_utility:apcu-status
     * @aliases cache_utility:apcu,cu:apcu,cu:apcu-status
     * @return RowsOfFields
     */
    public function apcu_status($options = ['host' => NULL])
    {
        $host = $this->getHost($options);
        if (!$host) {
            return 1;
        }

        $config = Drupal::config('cache_utility.settings');
        $accessKey = $config->get("security.accessKey");

        $resp = $this->makeJSONRequest($host, "cache_utility.apcu.status", $accessKey);
        if (!$resp) {
            return 1;
        }

        foreach ($resp['apcu_status'] as $key => $val) {
            if ($key == "scripts") {
                continue;
            }
            $rows[] = [
                'property' => $key,
                'value' => $val
            ];
        }
        return new RowsOfFields($rows);
    }


    /**
     * View the config of OP Cache.
     *
     * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
     *
     * @field-labels
     *   group: Group
     *   property: Property
     *   value: Value
     * @default-fields group,property,value
     *
     * @filter-default-field group
     * @usage cache_utility:opcache-config
     * @command cache_utility:opcache-config
     * @aliases cu:opcache-config
     * @return RowsOfFields
     */
    public function opcache_config($options = ['host' => NULL])
    {
        $host = $this->getHost($options);
        if (!$host) {
            return 1;
        }

        $config = Drupal::config('cache_utility.settings');
        $accessKey = $config->get("security.accessKey");

        $resp = $this->makeJSONRequest($host, "cache_utility.opcache.config", $accessKey);
        if (!$resp) {
            return 1;
        }

        foreach ($resp['opcache_config']['directives'] as $property => $value) {
            $rows[] = [
                'group' => 'directive',
                'property' => $property,
                'value' => $value,
            ];
        }

        foreach ($resp['opcache_config']['version'] as $property => $value) {
            $rows[] = [
                'group' => 'version',
                'property' => $property,
                'value' => $value,
            ];
        }

        return new RowsOfFields($rows);
    }

    /**
     * View the config of APCu.
     *
     * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
     *
     * @field-labels
     *   property: Property
     *   value: Value
     * @default-fields property,value
     *
     * @filter-default-field property
     * @usage cache_utility:apcu-config
     * @command cache_utility:apcu-config
     * @aliases cu:apcu-config
     * @return RowsOfFields
     */
    public function apcu_config($options = ['host' => NULL])
    {
        $host = $this->getHost($options);
        if (!$host) {
            return 1;
        }

        $config = Drupal::config('cache_utility.settings');
        $accessKey = $config->get("security.accessKey");

        $resp = $this->makeJSONRequest($host, "cache_utility.apcu.config", $accessKey);
        if (!$resp) {
            return 1;
        }

        foreach ($resp['apcu_config'] as $property => $value) {
            $rows[] = [
                'property' => $property,
                'value' => $value,
            ];
        }

        return new RowsOfFields($rows);
    }

    /**
     * Clear Cachetags.
     *
     * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
     * @usage cache_utility:cachetags-truncate
     * @command cache_utility:cachetags-truncate
     * @aliases cu:cachetags-truncate
     */
    public function cachetags_clear($options = ['host' => NULL])
    {
        $host = $this->getHost($options);
        if (!$host) {
            return 1;
        }

        $config = Drupal::config('cache_utility.settings');
        $accessKey = $config->get("security.accessKey");

        $resp = $this->makeJSONRequest($host, "cache_utility.cachetags.clear", $accessKey);
        if (!$resp) {
            return 1;
        }

        if ($resp['success']) {
            $num_rows = $resp['num_cachetag_rows_cleared'];
            $this->logger()->success(dt("The 'cachetags' table has been cleared ($num_rows rows deleted)."));
        } else {
            $this->logger()->success(dt("The 'cachetags' table could not be cleared."));
        }
        return 0;
    }

    /**
     * View Cachetags status.
     *
     * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
     * @usage cache_utility:cachetags-status
     * @command cache_utility:cachetags-status
     * @aliases cache_utility:cachetags,cu:cachetags-status,cu:cachetags
     */
    public function cachetags_status($options = ['host' => NULL])
    {
        $host = $this->getHost($options);
        if (!$host) {
            return 1;
        }

        $config = Drupal::config('cache_utility.settings');
        $accessKey = $config->get("security.accessKey");

        $resp = $this->makeJSONRequest($host, "cache_utility.cachetags.status", $accessKey);
        if (!$resp) {
            return 1;
        }

        if ($resp['success']) {
            $num_rows = $resp['num_cachetag_rows'];
            $this->logger()->success(dt("The 'cachetags' table has $num_rows rows."));
        } else {
            $this->logger()->success(dt("Could not count the total number of rows in the 'cachetags' tables."));
        }
        return 0;
    }

    /**
     * View cache_* tables status.
     *
     * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
     * @usage cache_utility:cachetables-status
     * @command cache_utility:cachetables-status
     * @aliases cache_utility:cachetables,cu:cachetables-status,cu:cachetables
     */
    public function cachetables_status($options = ['host' => NULL])
    {
        $host = $this->getHost($options);
        if (!$host) {
            return 1;
        }

        $config = Drupal::config('cache_utility.settings');
        $accessKey = $config->get("security.accessKey");

        $resp = $this->makeJSONRequest($host, "cache_utility.drupalcache.status", $accessKey);
        if (!$resp) {
            return 1;
        }

        if ($resp['success']) {
            $num_rows = $resp['num_cache_table_rows'];
            $this->logger()->success(dt("The 'cache_*' tables have a total of $num_rows rows."));
        } else {
            $this->logger()->success(dt("Could not count the total number of rows in all 'cache_*' tables."));
        }
        return 0;
    }

    /**
     * Truncate cache_* tables.
     *
     * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
     * @usage cache_utility:cachetables-truncate
     * @command cache_utility:cachetables-truncate
     * @aliases cu:cachetables-truncate
     */
    public function cachetables_truncate($options = ['host' => NULL])
    {
        $host = $this->getHost($options);
        if (!$host) {
            return 1;
        }

        $config = Drupal::config('cache_utility.settings');
        $accessKey = $config->get("security.accessKey");

        $resp = $this->makeJSONRequest($host, "cache_utility.drupalcachetables.clear", $accessKey);
        if (!$resp) {
            return 1;
        }

        if ($resp['success']) {
            $num_rows = $resp['num_deleted_cache_table_rows'];
            $this->logger()->success(dt("Truncated all 'cache_*' tables. Deleted a total of $num_rows rows."));
        } else {
            $this->logger()->success(dt("Could not truncate all 'cache_*' tables."));
        }
        return 0;
    }


    /**
     * Clear OP Cache.
     *
     * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
     * @usage cache_utility:opcache-clear
     * @command cache_utility:opcache-clear
     * @aliases cu:opcache-clear
     */
    public function opcache_clear($options = ['host' => NULL])
    {
        $host = $this->getHost($options);
        if (!$host) {
            return 1;
        }

        $config = Drupal::config('cache_utility.settings');
        $accessKey = $config->get("security.accessKey");

        $resp = $this->makeJSONRequest($host, "cache_utility.opcache.clear", $accessKey);
        if (!$resp) {
            return 1;
        }

        if ($resp['opcache_cleared']) {
            $this->logger()->success(dt("OP Cache has been cleared."));
        } else {
            $this->logger()->success(dt("OP Cache could not be cleared."));
        }
        return 0;
    }

    /**
     * Clear APCu.
     *
     * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
     * @usage cache_utility:apcu-clear
     * @command cache_utility:apcu-clear
     * @aliases cu:apcu-clear
     */
    public function apcu_clear($options = ['host' => NULL])
    {
        $host = $this->getHost($options);
        if (!$host) {
            return 1;
        }

        $config = Drupal::config('cache_utility.settings');
        $accessKey = $config->get("security.accessKey");

        $resp = $this->makeJSONRequest($host, "cache_utility.apcu.clear", $accessKey);
        if (!$resp) {
            return 1;
        }

        if ($resp['apcu_cleared']) {
            $this->logger()->success(dt("APCu has been cleared."));
        } else {
            $this->logger()->success(dt("APCu could not be cleared."));
        }
        return 0;
    }
}