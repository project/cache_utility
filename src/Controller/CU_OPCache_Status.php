<?php

namespace Drupal\cache_utility\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class CU_OPCache_Status extends ControllerBase
{

    /**
     * Controller handler for getting OPCache status
     * @return JsonResponse
     */
    public function getOPCacheStatus()
    {
        $request = Drupal::request();
        $accessKey = $request->headers->get("CU-ACCESS-KEY");
        if (!$accessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        $config = Drupal::config('cache_utility.settings');
        $correctAccessKey = $config->get('security.accessKey');

        if ($accessKey != $correctAccessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        // At this point, request is authenticated
        if (!$this->isOPCacheEnabled()) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'OPCache is not enabled.'
            ]);
        }

        // At this point, OP cache is enabled, so get its status
        $opcache_status = $this->getOPCache_Status();

        return new JsonResponse([
            'success' => TRUE,
            'opcache_status' => $opcache_status
        ]);
    }

    /**
     * Get OPCache status
     **/
    public static function getOPCache_Status()
    {
        $opcache_status = opcache_get_status();
        if (in_array("scripts", $opcache_status)) {
            unset($opcache_status['scripts']);
        }
        return $opcache_status;
    }

    /**
     * Check if OPCache is installed and enabled
     * @return bool
     */
    public static function isOPCacheEnabled()
    {
        try {
            return function_exists("opcache_get_status") && is_array(opcache_get_status()) ? TRUE : FALSE;
        } catch (\Error $e) {
            return FALSE;
        }
    }
}
