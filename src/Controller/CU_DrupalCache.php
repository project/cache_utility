<?php

namespace Drupal\cache_utility\Controller;

use Drupal;
use Drupal\Core\Database\Database;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class CU_DrupalCache extends ControllerBase
{
    /**
     * Controller handler for flushing Drupal cache
     * @return JsonResponse
     */
    public function clearDrupalCache()
    {
        $request = Drupal::request();
        $accessKey = $request->headers->get("CU-ACCESS-KEY");
        if (!$accessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        $config = Drupal::config('cache_utility.settings');
        $correctAccessKey = $config->get('security.accessKey');

        if ($accessKey != $correctAccessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        // At this point, request is authenticated
        $preclear_numrows = $this->getNumRowsInAllCacheTables();
        $this->clearAllDrupalCache();
        $afterclear_numrows = $this->getNumRowsInAllCacheTables();

        $num_affected_cachetablerows = $preclear_numrows - $afterclear_numrows;

        return new JsonResponse([
            'success' => TRUE,
            'num_deleted_cache_table_rows' => $num_affected_cachetablerows
        ]);
    }

    /**
     * Get the number of rows across all cache_* tables
     */
    public static function getNumRowsInAllCacheTables()
    {
        $total_num_cachetable_rows = 0;
        $tables = Drupal::database()->schema()->findTables("cache_%");

        foreach ($tables as $table_id => $table_name) {
            if (substr($table_id, 0, strlen("cache_")) !== "cache_") {
                continue;
            }
            $query = Drupal::database()->select($table_id);
            $num_table_rows = $query->countQuery()->execute()->fetchField();
            $total_num_cachetable_rows += $num_table_rows;
        }
        return $total_num_cachetable_rows;
    }

    /**
     * Flush Drupal cache
     */
    public static function clearAllDrupalCache()
    {
        drupal_flush_all_caches();
    }

    public function truncateDrupalCacheTables()
    {
        /*
         * FindTable does not work with sqlite see issue
         * https://www.drupal.org/project/drupal/issues/2949229
         * Workaround is to handle sqlite as a special case
         */
        $request = Drupal::request();
        $accessKey = $request->headers->get("CU-ACCESS-KEY");
        if (!$accessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        $config = Drupal::config('cache_utility.settings');
        $correctAccessKey = $config->get('security.accessKey');

        if ($accessKey != $correctAccessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        // At this point, request is authenticated
        $num_deleted_cache_table_rows = $this->getNumRowsInAllCacheTables();
        $this->truncateAllDrupalCacheTables();

        return new JsonResponse([
            'success' => TRUE,
            'num_deleted_cache_table_rows' => $num_deleted_cache_table_rows
        ]);
    }

    /**
     * Truncate all cache_* tables
     * @return bool
     */
    public static function truncateAllDrupalCacheTables()
    {
        $tables = Drupal::database()->schema()->findTables("cache_%");
        foreach ($tables as $table_id => $table_name) {
            if (substr($table_id, 0, strlen("cache_")) !== "cache_") {
                continue;
            }
            Drupal::database()->truncate($table_id)->execute();
        }
    }

    /**
     * Controller handler for getting the status on all cache_* tables
     * @return JsonResponse
     */
    public function getCacheTablesStatus()
    {
        $request = Drupal::request();
        $accessKey = $request->headers->get("CU-ACCESS-KEY");
        if (!$accessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        $config = Drupal::config('cache_utility.settings');
        $correctAccessKey = $config->get('security.accessKey');

        if ($accessKey != $correctAccessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        // At this point, request is authenticated
        return new JsonResponse([
            'success' => TRUE,
            'num_cache_table_rows' => $this->getNumRowsInAllCacheTables()
        ]);
    }
}
