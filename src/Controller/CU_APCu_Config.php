<?php

namespace Drupal\cache_utility\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class CU_APCu_Config extends ControllerBase
{

    /**
     * Controller handler for getting APCu config
     * @return JsonResponse
     */
    public function getCacheConfig()
    {
        $request = \Drupal::request();
        $accessKey = $request->headers->get("CU-ACCESS-KEY");
        if (!$accessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        $config = \Drupal::config('cache_utility.settings');
        $correctAccessKey = $config->get('security.accessKey');

        if ($accessKey != $correctAccessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        // At this point, request is authenticated
        if (!$this->isAPCuEnabled()) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'APCu is not enabled.'
            ]);
        }

        // At this point, cache is enabled, so get its config
        $cache_config = $this->getAPCuConfig();

        return new JsonResponse([
            'success' => TRUE,
            'apcu_config' => $cache_config
        ]);
    }

    /**
     * Get APCu Config
     * @return array|false
     */
    public static function getAPCuConfig() {
        return apcu_sma_info(TRUE);
    }

    /**
     * Check if APCu is installed and enabled
     * @return bool
     */
    public static function isAPCuEnabled()
    {
        try {
            return function_exists("apcu_enabled") && apcu_enabled();
        } catch (\Error $e) {
            return FALSE;
        }
    }
}
