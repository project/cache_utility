<?php

namespace Drupal\cache_utility\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class CU_APCu_Clear extends ControllerBase
{

    /**
     * Controller handler for clearing APCu cache
     * @return JsonResponse
     */
    public function clearCache()
    {
        $request = \Drupal::request();
        $accessKey = $request->headers->get("CU-ACCESS-KEY");
        if (!$accessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        $config = \Drupal::config('cache_utility.settings');
        $correctAccessKey = $config->get('security.accessKey');

        if ($accessKey != $correctAccessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        // At this point, request is authenticated
        if (!$this->isAPCuEnabled()) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'APCu is not enabled.'
            ]);
        }

        // At this point, cache is enabled, so clear it
        $cache_cleared = $this->clearAPCuCache();

        return new JsonResponse([
            'success' => TRUE,
            'apcu_cleared' => $cache_cleared
        ]);
    }

    /**
     * Clear APCu Cache
     */
    public static function clearAPCuCache() {
        return apcu_clear_cache();
    }

    /**
     * Check if APCu is installed and enabled
     * @return bool
     */
    public static function isAPCuEnabled()
    {
        try {
            return function_exists("apcu_enabled") && apcu_enabled();
        } catch (\Error $e) {
            return FALSE;
        }
    }
}
