<?php

namespace Drupal\cache_utility\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class CU_OPCache_Clear extends ControllerBase
{

    /**
     * Controller handler for clearing OPCache
     * @return JsonResponse
     */
    public function clearOPCache()
    {
        $request = \Drupal::request();
        $accessKey = $request->headers->get("CU-ACCESS-KEY");
        if (!$accessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        $config = \Drupal::config('cache_utility.settings');
        $correctAccessKey = $config->get('security.accessKey');

        if ($accessKey != $correctAccessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        // At this point, request is authenticated
        if (!$this->isOPCacheEnabled()) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'OPCache is not enabled.'
            ]);
        }

        // At this point, OP cache is enabled, so clear it
        $opcache_cleared = $this->resetOPCache();

        return new JsonResponse([
            'success' => TRUE,
            'opcache_cleared' => $opcache_cleared
        ]);
    }

    /**
     * Reset OP Cache
     */
    public static function resetOPCache() {
        return opcache_reset();
    }

    /**
     * Check if OPCache is installed and enabled
     * @return bool
     */
    public static function isOPCacheEnabled()
    {
        try {
            return function_exists("opcache_get_status") && is_array(opcache_get_status()) ? TRUE : FALSE;
        } catch (\Error $e) {
            return FALSE;
        }
    }
}
