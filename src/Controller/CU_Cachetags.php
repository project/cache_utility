<?php

namespace Drupal\cache_utility\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class CU_Cachetags extends ControllerBase
{
    /**
     * Controller handler for clearing the cachetags table
     * @return JsonResponse
     */
    public function clearCachetags()
    {
        $request = \Drupal::request();
        $accessKey = $request->headers->get("CU-ACCESS-KEY");
        if (!$accessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        $config = \Drupal::config('cache_utility.settings');
        $correctAccessKey = $config->get('security.accessKey');

        if ($accessKey != $correctAccessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        // At this point, request is authenticated
        if ($this->doesCachetagsTableExist()) {
            $num_cachetag_rows = $this->getNumRowsInCachetagsTable();
            $this->clearCachetagsTable();

            return new JsonResponse([
                'success' => TRUE,
                'num_cachetag_rows_cleared' => $num_cachetag_rows
            ]);
        }

        return new JsonResponse([
            'success' => FALSE
        ]);
    }

    /**
     * Controller handler for getting the status on the cachetags table
     * @return JsonResponse
     */
    public function getCachetagsStatus()
    {
        $request = \Drupal::request();
        $accessKey = $request->headers->get("CU-ACCESS-KEY");
        if (!$accessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        $config = \Drupal::config('cache_utility.settings');
        $correctAccessKey = $config->get('security.accessKey');

        if ($accessKey != $correctAccessKey) {
            return new JsonResponse([
                'success' => FALSE,
                'error' => 'Access denied.',
            ]);
        }

        // At this point, request is authenticated
        if ($this->doesCachetagsTableExist()) {
            return new JsonResponse([
                'success' => TRUE,
                'num_cachetag_rows' => $this->getNumRowsInCachetagsTable()
            ]);
        }

        return new JsonResponse([
            'success' => FALSE
        ]);
    }

    /**
     * Check if the cachetags table exists
     * @return bool
     */
    public static function doesCachetagsTableExist() {
        return \Drupal::database()->schema()->tableExists("cachetags");
    }

    /**
     * Get the number of rows in the cachetags table
     */
    public static function getNumRowsInCachetagsTable() {
        $query = \Drupal::database()->select('cachetags');
        $num_rows = $query->countQuery()->execute()->fetchField();
        return $num_rows;
    }

    /**
     * Truncate the cachetags table
     */
    public static function clearCachetagsTable() {
        \Drupal::database()->truncate('cachetags')->execute();
    }
}
