<?php

namespace Drupal\cache_utility\Form;

use Drupal\cache_utility\Controller\CU_Cachetags;
use Drupal\cache_utility\Controller\CU_DrupalCache;
use Drupal\cache_utility\Controller\CU_OPCache_Clear;
use Drupal\cache_utility\Controller\CU_APCu_Clear;
use Drupal\cache_utility\Controller\CU_OPCache_Config;
use Drupal\cache_utility\Controller\CU_APCu_Config;
use Drupal\cache_utility\Controller\CU_OPCache_Status;
use Drupal\cache_utility\Controller\CU_APCu_Status;
use Drupal;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;

/**
 * cache_utility settingsForm.
 */
class SettingsForm extends ConfigFormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'cache_utility.settings';
    }

    /**
     * Rebuild the form
     *
     * @param $form
     * @param $form_state
     */
    public function submitCallback(&$form, &$form_state)
    {
        $form_state->setRebuild(TRUE);
    }

    /**
     * Clear OPCache
     */
    public function clearOPCache(&$form, &$form_state)
    {
        CU_OPCache_Clear::resetOPCache();
        Drupal::messenger()->addMessage("OPCache has been cleared.");
        return $form['settings'];
    }

    /**
     * Clear APCu cache
     */
    public function clearAPCuCache(&$form, &$form_state)
    {
        CU_APCu_Clear::clearAPCuCache();
        Drupal::messenger()->addMessage("APCu has been cleared.");
        return $form['settings'];
    }

    /**
     * Get OPCache config as a formatted string
     */
    public function getOPCacheConfig(&$form, &$form_state)
    {
        $opcache_config = CU_OPCache_Config::getOPCacheConfiguration();
        $config_str = "";
        foreach ($opcache_config['directives'] as $property => $value) {
            if ($value === FALSE) {
                $value = "false";
            }
            $config_str .= "$property: $value<br/>";
        }

        $config_str .= "<br/>";
        foreach ($opcache_config['version'] as $property => $value) {
            if ($value === FALSE) {
                $value = "false";
            }
            $config_str .= "$property: $value<br/>";
        }

        Drupal::messenger()->addMessage(t($config_str));
        return $form['settings'];
    }

    /**
     * Get APCu cache config as a formatted string
     */
    public function getAPCuCacheConfig(&$form, &$form_state)
    {
        $apcu_config = CU_APCu_Config::getAPCuConfig();
        $config_str = "";
        foreach ($apcu_config as $property => $value) {
            if ($value === FALSE) {
                $value = "false";
            }
            $config_str .= "$property: $value<br/>";
        }
        Drupal::messenger()->addMessage(t($config_str));
        return $form['settings'];
    }

    /**
     * Clear the cachetags table
     */
    public function clearDrupalcachetags(&$form, &$form_state)
    {
        $config_str = "";

        if (CU_Cachetags::doesCachetagsTableExist()) {
            $num_cachetag_rows = CU_Cachetags::getNumRowsInCachetagsTable();
            CU_Cachetags::clearCachetagsTable();
            $config_str = "Cleared Drupal 'cachetags' table (deleted $num_cachetag_rows rows).";
        } else {
            $config_str = "The 'cachetags' table does not exist.";
        }

        Drupal::messenger()->addMessage(t($config_str));
        return $form['settings'];
    }

    /**
     * Flush Drupal cache
     */
    public function clearDrupalCache(&$form, &$form_state)
    {
        drupal_flush_all_caches();
        Drupal::messenger()->addMessage(t("Drupal cache has been cleared."));
        return $form['settings'];
    }

    /**
     * Truncate all cache_* tables
     */
    public function clearDrupalcachetables(&$form, &$form_state)
    {
        $num_rows = CU_DrupalCache::getNumRowsInAllCacheTables();
        CU_DrupalCache::truncateAllDrupalCacheTables();
        Drupal::messenger()->addMessage(t("Deleted $num_rows across all cache_* tables."));
        return $form['settings'];
    }

    /**
     * Get OPCache status
     */
    public function getOPCacheStatus(&$form, &$form_state)
    {
        $opcache_status = CU_OPCache_Status::getOPCache_Status();
        $status_str = "";
        foreach ($opcache_status as $key => $val) {
            if ($key == "scripts") {
                continue;
            }
            if (in_array($key, ['opcache_enabled', 'cache_full', 'restart_pending', 'restart_in_progress'])) {
                if ($val === FALSE) {
                    $val = "false";
                }
                $status_str .= "$key: $val<br/>";
            } else {
                $status_str .= "<br/>$key<br/>";
                foreach ($val as $sub_key => $sub_val) {
                    if ($sub_val === FALSE) {
                        $sub_val = "false";
                    }
                    $status_str .= "$sub_key: $sub_val<br/>";
                }
            }
        }
        Drupal::messenger()->addMessage(t($status_str));
        return $form['settings'];
    }

    /**
     * Get APCu cache status
     */
    public function getAPCuCacheStatus(&$form, &$form_state)
    {
        $apcu_status = CU_APCu_Status::getAPCuStatus();
        $status_str = "";
        foreach ($apcu_status as $key => $val) {
            if ($val === FALSE) {
                $val = "false";
            }
            $status_str .= "$key: $val<br/>";
        }
        Drupal::messenger()->addMessage(t($status_str));
        return $form['settings'];
    }

    /**
     * Ajax handler for the "showHTTPSHosts" checkbox
     */
    public function _update_showHTTPSHosts(&$form, $form_state) {
        return $form['settings'];
    }

    /**
     * Ajax handler for the "skip_ssl_verification" checkbox
     */
    public function _update_skip_ssl_verification(&$form, $form_state) {
        return $form['settings'];
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('cache_utility.settings');

        $host = Drupal::request()->getSchemeAndHttpHost();
        $accessKey = $config->get("security.accessKey");

        $opcache_enabled = CU_OPCache_Config::isOPCacheEnabled();
        $apcu_enabled = CU_APCu_Config::isAPCuEnabled();

        $form['settings'] = array(
            '#tree' => TRUE,
            '#prefix' => '<div id="cu-settings-form-ajax">',
            '#suffix' => '</div>',
            '#type' => 'container',
            '#attributes' => [
                'id' => 'box-container',
            ],
        );

        // security
        $form['settings']['security']['security_accessKey'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Access key'),
            '#default_value' => $config->get("security.accessKey"),
            '#maxlength' => 1024,
            '#required' => TRUE,
            '#description' => t("The access key is passed as a HTTP header to grant access to this module's API routes for flushing cache, getting configuration and cache status of OPCache, and APCu.<br/>See drush & cURL command usage."),
        ];

        $form['settings']['separator'] = [
            '#markup' => $this->t("<br/>"),
        ];

        $showHTTPSHosts = $form_state->getValue(['settings','showHTTPSHosts']);
        if ($showHTTPSHosts === NULL) {
            $showHTTPSHosts = TRUE;
        }

        if ($showHTTPSHosts) {
            $host = str_replace("http://", "https://", $host);
            $scheme = "https://";
        } else {
            $host = str_replace("https://", "http://", $host);
            $scheme = "http://";
        }

        $skip_ssl_verification = $form_state->getValue(['settings','skip_ssl_verification']);
        if ($skip_ssl_verification === NULL) {
            $skip_ssl_verification = $config->get('skip_ssl_verification');
        }

        $form['settings']['showHTTPSHosts'] = [
            '#type' => 'checkbox',
            '#title' => $this->t("Force curl and drush commands to show urls with https, instead of http"),
            '#description' => $this->t("Helpful to avoid inadvertent use of insecure http addresses for sites running behind https proxy"),
            '#ajax' => array(
                'callback' => array($this, '_update_showHTTPSHosts'),
                'wrapper' => 'box-container',
            ),
            '#default_value' => $showHTTPSHosts,
        ];


        $form['settings']['skip_ssl_verification'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Force curl and drush commands to skip verification of SSL certificates for the host'),
            '#description' => $this->t('This should only be used if the host URL does not have proper SSL certificates configured, or if they are internal ones that cannot be resolved properly by external requests'),
            '#ajax' => array(
                'callback' => array($this, '_update_skip_ssl_verification'),
                'wrapper' => 'box-container',
            ),
            '#default_value' => $skip_ssl_verification, //$config->get('skip_ssl_verification'),
        ];

        $form['settings']["flush_caches"]['header'] = [
            '#markup' => $this->t("<br/><h4>Flush the following caches when Drupal's cache is cleared</h4>"),
        ];

        if ($opcache_enabled) {
            $form['settings']['flush_caches']['opcache'] = [
                '#type' => 'checkbox',
                '#title' => $this->t('OPCache'),
                '#default_value' => $config->get("flushCaches.opcache"),
                '#required' => FALSE,
            ];
        }

        if ($apcu_enabled) {
            $form['settings']['flush_caches']['apcu'] = [
                '#type' => 'checkbox',
                '#title' => $this->t('APCu'),
                '#default_value' => $config->get("flushCaches.apcu"),
                '#required' => FALSE,
            ];
        }

        if (CU_Cachetags::doesCachetagsTableExist()) {
            $form['settings']['flush_caches']['drupal_cachetags'] = [
                '#type' => 'checkbox',
                '#title' => $this->t("Database cachetags table"),
                '#default_value' => $config->get("flushCaches.drupal_cachetags"),
                '#required' => FALSE,
            ];
        }

        $form['settings']['flush_caches']['drupal_cachetables'] = [
            '#type' => 'checkbox',
            '#title' => $this->t("Database cache_* tables"),
            '#default_value' => $config->get("flushCaches.drupal_cachetables"),
            '#required' => FALSE,
        ];

        $form['settings']["flush_caches"]['separator'] = [
            '#markup' => $this->t("<br/>"),
        ];

        $curl_prefix = "curl";
        if ($skip_ssl_verification) {
            $curl_prefix = "curl --insecure";
        }

        // OPCache
        $form['settings']['opcache']['controls']['status_header'] = [
            '#markup' => "<br/><div><h4>OPCache</h4></div>"
        ];
        if (!$opcache_enabled) {
            $form['settings']['opcache']['controls']['status'] = [
                "#markup" => "<div>Extension not found or disabled</div><br/>"
            ];
        } else {
            $form['settings']['opcache']['controls']['display'] = [
                '#type' => 'details',
                '#open' => FALSE,
                '#title' => $this->t('OPCache Usage'),
            ];
            $form['settings']['opcache']['controls']['display']['info'] = [
                '#markup' => $this->getOPCacheDisplayInfo()
            ];

            // Drush & curl commands
            $form['settings']['opcache']['commands']['display'] = [
                '#type' => 'details',
                '#open' => FALSE,
                '#title' => $this->t('Drush & Curl commands'),
            ];

            // OPCache clear
            $path = Url::fromRoute('cache_utility.opcache.clear')->toString();
            $form['settings']['opcache']['commands']['display']['clear']['header'] = [
                '#markup' => $this->t("<h4>Clear OPCache</h4>"),
            ];
            $form['settings']['opcache']['commands']['display']['clear']['drush'] = [
                '#markup' => $this->t("<div><b>Drush:&nbsp;&nbsp;&nbsp;</b><span><code>drush cu:opcache-clear --host $host</code></span></div>"),
            ];
            $form['settings']['opcache']['commands']['display']['clear']['curl'] = [
                '#markup' => $this->t("<div><b>Curl (site host):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' $host$path</code></span></div>"),
            ];
            $form['settings']['opcache']['commands']['display']['clear']['curl_localhost'] = [
                '#markup' => $this->t("<div><b>Curl (localhost):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' http://localhost$path</code></span></div>"),
            ];
            $form['settings']['opcache']['commands']['display']['clear']['separator'] = [
                '#markup' => $this->t("<br/>"),
            ];

            // OPCache config
            $path = Url::fromRoute('cache_utility.opcache.config')->toString();
            $form['settings']['opcache']['commands']['display']['config']['header'] = [
                '#markup' => $this->t("<h4>Get OPCache config</h4>"),
            ];

            $form['settings']['opcache']['commands']['display']['config']['drush'] = [
                '#markup' => $this->t("<div><b>Drush:&nbsp;&nbsp;&nbsp;</b><span><code>drush cu:opcache-config --host $host</code></span></div>"),
            ];
            $form['settings']['opcache']['commands']['display']['config']['curl'] = [
                '#markup' => $this->t("<div><b>Curl (site host):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' $host$path</code></span></div>"),
            ];
            $form['settings']['opcache']['commands']['display']['config']['curl_localhost'] = [
                '#markup' => $this->t("<div><b>Curl (localhost):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' http://localhost$path</code></span></div>"),
            ];
            $form['settings']['opcache']['commands']['display']['config']['separator'] = [
                '#markup' => $this->t("<br/>"),
            ];

            // OPCache status
            $path = Url::fromRoute('cache_utility.opcache.status')->toString();
            $form['settings']['opcache']['commands']['display']['status']['header'] = [
                '#markup' => $this->t("<h4>Get OPCache status</h4>"),
            ];
            $form['settings']['opcache']["commands"]['display']['status']['drush'] = [
                '#markup' => $this->t("<div><b>Drush:&nbsp;&nbsp;&nbsp;</b><span><code>drush cu:opcache-status --host $host</code></span></div>"),
            ];
            $form['settings']['opcache']["commands"]['display']['status']['curl'] = [
                '#markup' => $this->t("<div><b>Curl (site host):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' $host$path</code></span></div>"),
            ];
            $form['settings']['opcache']["commands"]['display']['status']['curl_localhost'] = [
                '#markup' => $this->t("<div><b>Curl (localhost):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' http://localhost$path</code></span></div>"),
            ];
            $form['settings']['opcache']["commands"]['display']['status']['separator'] = [
                '#markup' => $this->t("<br/>"),
            ];


            // OPCache Buttons
            $form['settings']['opcache']['commands']['buttons']['clear']['submit'] = [
                '#type' => 'submit',
                '#name' => 'clear_opcache',
                '#value' => t('Clear OPCache'),
                '#submit' => array(array($this, 'submitCallback')),
                '#ajax' => array(
                    'callback' => array($this, 'clearOPCache'),
                    'wrapper' => 'cu-settings-form-ajax',
                ),
            ];
            $form['settings']['opcache']["commands"]['buttons']['config']['submit'] = [
                '#type' => 'submit',
                '#name' => 'config_opcache',
                '#value' => t('Get OPCache config'),
                '#submit' => array(array($this, 'submitCallback')),
                '#ajax' => array(
                    'callback' => array($this, 'getOPCacheConfig'),
                    'wrapper' => 'cu-settings-form-ajax',
                ),
            ];
            $form['settings']['opcache']["commands"]['buttons']['status']['submit'] = [
                '#type' => 'submit',
                '#name' => 'status_opcache',
                '#value' => t('Get OPCache status'),
                '#submit' => array(array($this, 'submitCallback')),
                '#ajax' => array(
                    'callback' => array($this, 'getOPCacheStatus'),
                    'wrapper' => 'cu-settings-form-ajax',
                ),
            ];

            $form['settings']['opcache']["commands"]['buttons']['separator'] = [
                '#markup' => '<br/><br/>'
            ];
        }

        $form['settings']["commands"][] = [
            '#markup' => $this->t("<hr/><br/>"),
        ];

        // APCu
        $form['settings']['apcu']['controls']['status_header'] = [
            '#markup' => "<div><h4>APCu</h4></div>"
        ];

        if (!$apcu_enabled) {
            $form['settings']['apcu']['controls']['status'] = [
                "#markup" => "<div>Extension not found or disabled</div><br/>"
            ];
        } else {
            $form['settings']['apcu']['controls']['display'] = [
                '#type' => 'details',
                '#open' => FALSE,
                '#title' => $this->t('APCu Usage'),
            ];
            $form['settings']['apcu']['controls']['display']['info'] = [
                '#markup' => $this->getAPCuCacheDisplayInfo()
            ];

            // Drush & curl commands
            $form['settings']['apcu']['commands']['display'] = [
                '#type' => 'details',
                '#open' => FALSE,
                '#title' => $this->t('Drush & Curl commands'),
            ];


            // APCu clear
            $path = Url::fromRoute('cache_utility.apcu.clear')->toString();
            $form['settings']['apcu']['commands']['display']['clear']['header'] = [
                '#markup' => $this->t("<h4>Clear APCu</h4>"),
            ];
            $form['settings']['apcu']["commands"]['display']['clear']['drush'] = [
                '#markup' => $this->t("<div><b>Drush:&nbsp;&nbsp;&nbsp;</b><span><code>drush cu:apcu-clear --host $host</code></span></div>"),
            ];
            $form['settings']['apcu']["commands"]['display']['clear']['curl'] = [
                '#markup' => $this->t("<div><b>Curl (site host):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' $host$path</code></span></div>"),
            ];
            $form['settings']['apcu']["commands"]['display']['clear']['curl_localhost'] = [
                '#markup' => $this->t("<div><b>Curl (localhost):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' http://localhost$path</code></span></div>"),
            ];
            $form['settings']['apcu']["commands"]['display']['clear']['separator'] = [
                '#markup' => $this->t("<br/>"),
            ];

            // APCu config
            $path = Url::fromRoute('cache_utility.apcu.config')->toString();
            $form['settings']['apcu']['commands']['display']['config']['header'] = [
                '#markup' => $this->t("<h4>Get APCu config</h4>"),
            ];
            $form['settings']['apcu']["commands"]['display']['config']['drush'] = [
                '#markup' => $this->t("<div><b>Drush:&nbsp;&nbsp;&nbsp;</b><span><code>drush cu:apcu-config --host $host</code></span></div>"),
            ];
            $form['settings']['apcu']["commands"]['display']['config']['curl'] = [
                '#markup' => $this->t("<div><b>Curl (site host):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' $host$path</code></span></div>"),
            ];
            $form['settings']['apcu']["commands"]['display']['config']['curl_localhost'] = [
                '#markup' => $this->t("<div><b>Curl (localhost):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' http://localhost$path</code></span></div>"),
            ];
            $form['settings']['apcu']["commands"]['display']['config']['separator'] = [
                '#markup' => $this->t("<br/>"),
            ];

            // APCu status
            $path = Url::fromRoute('cache_utility.apcu.status')->toString();
            $form['settings']['apcu']['commands']['display']['status']['header'] = [
                '#markup' => $this->t("<h4>Get APCu status</h4>"),
            ];
            $form['settings']['apcu']["commands"]['display']['status']['drush'] = [
                '#markup' => $this->t("<div><b>Drush:&nbsp;&nbsp;&nbsp;</b><span><code>drush cu:apcu-status --host $host</code></span></div>"),
            ];
            $form['settings']['apcu']["commands"]['display']['status']['curl'] = [
                '#markup' => $this->t("<div><b>Curl (site host):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' $host$path</code></span></div>"),
            ];
            $form['settings']['apcu']["commands"]['display']['status']['curl_localhost'] = [
                '#markup' => $this->t("<div><b>Curl (localhost):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' http://localhost$path</code></span></div>"),
            ];
            $form['settings']['apcu']["commands"]['display']['status']['separator'] = [
                '#markup' => $this->t("<br/>"),
            ];

            // APCu buttons
            $form['settings']['apcu']["commands"]['buttons']['clear']['submit'] = [
                '#type' => 'submit',
                '#name' => 'apcu_opcache',
                '#value' => t('Clear APCu'),
                '#submit' => array(array($this, 'submitCallback')),
                '#ajax' => array(
                    'callback' => array($this, 'clearAPCuCache'),
                    'wrapper' => 'cu-settings-form-ajax',
                ),
            ];
            $form['settings']['apcu']["commands"]['buttons']['config']['submit'] = [
                '#type' => 'submit',
                '#name' => 'config_apcu',
                '#value' => t('Get APCu config'),
                '#submit' => array(array($this, 'submitCallback')),
                '#ajax' => array(
                    'callback' => array($this, 'getAPCuCacheConfig'),
                    'wrapper' => 'cu-settings-form-ajax',
                ),
            ];
            $form['settings']['apcu']["commands"]['buttons']['status']['submit'] = [
                '#type' => 'submit',
                '#name' => 'status_apcu',
                '#value' => t('Get APCu status'),
                '#submit' => array(array($this, 'submitCallback')),
                '#ajax' => array(
                    'callback' => array($this, 'getAPCuCacheStatus'),
                    'wrapper' => 'cu-settings-form-ajax',
                ),
            ];
            $form['settings']['apcu']["commands"]['buttons']['separator'] = [
                '#markup' => '<br/><br/>'
            ];
        }

        $form['settings']['apcu']["commands"]['separator'] = [
            '#markup' => '<hr/><br/>'
        ];


        // Drupal cache
        $form['settings']['drupal_cache']['controls']['status'] = [
            '#markup' => "<div><h4>Database cache tables</h4></div>"
        ];
        $form['settings']['drupal_cache']['controls']['display'] = [
            '#type' => 'details',
            '#open' => FALSE,
            '#title' => $this->t('Database Cache Tables Usage'),
        ];
        $form['settings']['drupal_cache']['controls']['display']['info'] = [
            '#markup' => $this->getDrupalCacheDisplayInfo()
        ];

        // Drush & curl commands
        $form['settings']['drupal_cache']['commands']['display'] = [
            '#type' => 'details',
            '#open' => FALSE,
            '#title' => $this->t('Drush & Curl commands'),
        ];

        // View Drupal cachetags status
        if (CU_Cachetags::doesCachetagsTableExist()) {
            $path = Url::fromRoute('cache_utility.cachetags.status')->toString();
            $form['settings']['drupal_cache']['commands']['display']['status_cachetags']['header'] = [
                '#markup' => $this->t("<h4>View the status of database cachetags table</h4>"),
            ];
            $form['settings']['drupal_cache']["commands"]['display']['status_cachetags']['drush'] = [
                '#markup' => $this->t("<div><b>Drush:&nbsp;&nbsp;&nbsp;</b><span><code>drush cu:cachetags-status --host $host</code></span></div>"),
            ];
            $form['settings']['drupal_cache']["commands"]['display']['status_cachetags']['curl'] = [
                '#markup' => $this->t("<div><b>Curl (site host):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' $host$path</code></span></div>"),
            ];
            $form['settings']['drupal_cache']["commands"]['display']['status_cachetags']['curl_localhost'] = [
                '#markup' => $this->t("<div><b>Curl (localhost):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' http://localhost$path</code></span></div>"),
            ];
            $form['settings']['drupal_cache']["commands"]['display']['status_cachetags']['separator'] = [
                '#markup' => $this->t("<br/>"),
            ];
        }

        // Clear Drupal cachetags
        if (CU_Cachetags::doesCachetagsTableExist()) {

            $path = Url::fromRoute('cache_utility.cachetags.clear')->toString();
            $form['settings']['drupal_cache']['commands']['display']['clear_cachetags']['header'] = [
                '#markup' => $this->t("<h4>Truncate database cachetags table</h4>"),
            ];
            $form['settings']['drupal_cache']["commands"]['display']['clear_cachetags']['drush'] = [
                '#markup' => $this->t("<div><b>Drush:&nbsp;&nbsp;&nbsp;</b><span><code>drush cu:cachetags-truncate --host $host</code></span></div>"),
            ];
            $form['settings']['drupal_cache']["commands"]['display']['clear_cachetags']['curl'] = [
                '#markup' => $this->t("<div><b>Curl (site host):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' $host$path</code></span></div>"),
            ];
            $form['settings']['drupal_cache']["commands"]['display']['clear_cachetags']['curl_localhost'] = [
                '#markup' => $this->t("<div><b>Curl (localhost):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' http://localhost$path</code></span></div>"),
            ];
            $form['settings']['drupal_cache']["commands"]['display']['clear_cachetags']['separator'] = [
                '#markup' => $this->t("<br/>"),
            ];
        }

        // View Drupal cache_* tables status
        $path = Url::fromRoute('cache_utility.drupalcache.status')->toString();
        $form['settings']['drupal_cache']['commands']['display']['status_cachetables']['header'] = [
            '#markup' => $this->t("<h4>View the status of database cache_* tables</h4>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['status_cachetables']['drush'] = [
            '#markup' => $this->t("<div><b>Drush:&nbsp;&nbsp;&nbsp;</b><span><code>drush cu:cachetables-status --host $host</code></span></div>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['status_cachetables']['curl'] = [
            '#markup' => $this->t("<div><b>Curl (site host):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' $host$path</code></span></div>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['status_cachetables']['curl_localhost'] = [
            '#markup' => $this->t("<div><b>Curl (localhost):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' http://localhost$path</code></span></div>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['status_cachetables']['separator'] = [
            '#markup' => $this->t("<br/>"),
        ];

        // Truncate drupal cache_* tables
        $path = Url::fromRoute('cache_utility.drupalcachetables.clear')->toString();
        $form['settings']['drupal_cache']['commands']['display']['clear_drupalcachetables']['header'] = [
            '#markup' => $this->t("<h4>Truncate all database cache_* tables</h4>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['clear_drupalcachetables']['drush'] = [
            '#markup' => $this->t("<div><b>Drush:&nbsp;&nbsp;&nbsp;</b><span><code>drush cu:cachetables-truncate --host $host</code></span></div>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['clear_drupalcachetables']['curl'] = [
            '#markup' => $this->t("<div><b>Curl (site host):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' $host$path</code></span></div>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['clear_drupalcachetables']['curl_localhost'] = [
            '#markup' => $this->t("<div><b>Curl (localhost):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' http://localhost$path</code></span></div>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['clear_drupalcachetables']['separator'] = [
            '#markup' => $this->t("<br/>"),
        ];

        // Clear Drupal Cache
        $path = Url::fromRoute('cache_utility.drupalcache.clear')->toString();
        $form['settings']['drupal_cache']['commands']['display']['clear_drupal_cache']['header'] = [
            '#markup' => $this->t("<h4>Clear Drupal's cache</h4>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['clear_drupal_cache']['drush'] = [
            '#markup' => $this->t("<div><b>Drush:&nbsp;&nbsp;&nbsp;</b><span><code>drush cr</code></span></div>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['clear_drupal_cache']['curl'] = [
            '#markup' => $this->t("<div><b>Curl (site host):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' $host$path</code></span></div>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['clear_drupal_cache']['curl_localhost'] = [
            '#markup' => $this->t("<div><b>Curl (localhost):&nbsp;&nbsp;&nbsp;</b><span><code>$curl_prefix -H 'Content-Type: application/json' -H 'CU-ACCESS-KEY: $accessKey' http://localhost$path</code></span></div>"),
        ];
        $form['settings']['drupal_cache']["commands"]['display']['clear_drupal_cache']['separator'] = [
            '#markup' => $this->t("<br/>"),
        ];


        // Drupal cache buttons
        if (CU_Cachetags::doesCachetagsTableExist()) {

            $form['settings']['drupal_cache']["commands"]['buttons']['clear']['submit'] = [
                '#type' => 'submit',
                '#name' => 'drupal_cachetags',
                '#value' => t('Truncate database cachetags table'),
                '#submit' => array(array($this, 'submitCallback')),
                '#ajax' => array(
                    'callback' => array($this, 'clearDrupalcachetags'),
                    'wrapper' => 'cu-settings-form-ajax',
                ),
            ];
        }


        $form['settings']['drupal_cache']["commands"]['buttons']['clear_cachetables']['submit'] = [
            '#type' => 'submit',
            '#name' => 'drupal_cachetables',
            '#value' => t('Truncate database cache_* tables'),
            '#submit' => array(array($this, 'submitCallback')),
            '#ajax' => array(
                'callback' => array($this, 'clearDrupalcachetables'),
                'wrapper' => 'cu-settings-form-ajax',
            ),
        ];


        $form['settings']['drupal_cache']["commands"]['buttons']['clear_drupal_cache']['submit'] = [
            '#type' => 'submit',
            '#name' => 'drupal_cache',
            '#value' => t('Clear Drupal\'s cache'),
            '#submit' => array(array($this, 'submitCallback')),
            '#ajax' => array(
                'callback' => array($this, 'clearDrupalCache'),
                'wrapper' => 'cu-settings-form-ajax',
            ),
        ];

        $form['settings']['drupal_cache']["commands"]['buttons']['separator'] = [
            '#markup' => '<br/><br/>'
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * Display info for OPCache
     */
    private function getOPCacheDisplayInfo()
    {
        // From: https://github.com/amnuts/opcache-gui/blob/master/index.php
        $status = opcache_get_status();
        $config = opcache_get_configuration();
        $missingConfig = array_diff_key(ini_get_all('zend opcache', false), $config['directives']);
        if (!empty($missingConfig)) {
            $config['directives'] = array_merge($config['directives'], $missingConfig);
        }

        $info = array_merge(
            $status['memory_usage'], $status['opcache_statistics'], [
                'used_memory_percentage' => round(100 * (
                        ($status['memory_usage']['used_memory'] + $status['memory_usage']['wasted_memory'])
                        / $config['directives']['opcache.memory_consumption']
                    )),
                'hit_rate_percentage' => round($status['opcache_statistics']['opcache_hit_rate']),
                'used_key_percentage' => round(100 * (
                        $status['opcache_statistics']['num_cached_keys']
                        / $status['opcache_statistics']['max_cached_keys']
                    )),
                'wasted_percentage' => round($status['memory_usage']['current_wasted_percentage'], 2),
                'readable' => [
                    'total_memory' => $this->size($config['directives']['opcache.memory_consumption']),
                    'used_memory' => $this->size($status['memory_usage']['used_memory']),
                    'free_memory' => $this->size($status['memory_usage']['free_memory']),
                    'wasted_memory' => $this->size($status['memory_usage']['wasted_memory']),
                    'num_cached_scripts' => number_format($status['opcache_statistics']['num_cached_scripts']),
                    'hits' => number_format($status['opcache_statistics']['hits']),
                    'misses' => number_format($status['opcache_statistics']['misses']),
                    'blacklist_miss' => number_format($status['opcache_statistics']['blacklist_misses']),
                    'num_cached_keys' => number_format($status['opcache_statistics']['num_cached_keys']),
                    'max_cached_keys' => number_format($status['opcache_statistics']['max_cached_keys']),
                    'interned' => null,
                    'start_time' => date('Y-m-d H:i:s', $status['opcache_statistics']['start_time']),
                    'last_restart_time' => ($status['opcache_statistics']['last_restart_time'] == 0
                        ? 'never'
                        : date('Y-m-d H:i:s', $status['opcache_statistics']['last_restart_time'])
                    )
                ]
            ]
        );

        if (!empty($status['preload_statistics']['scripts'])) {
            $preload = $status['preload_statistics']['scripts'];
            sort($preload, SORT_STRING);
            if ($info) {
                $info['preload_memory'] = $status['preload_statistics']['memory_consumption'];
                $info['readable']['preload_memory'] = $this->size($status['preload_statistics']['memory_consumption']);
            }
        }

        if (!empty($status['interned_strings_usage'])) {
            $info['readable']['interned'] = [
                'buffer_size' => $this->size($status['interned_strings_usage']['buffer_size']),
                'strings_used_memory' => $this->size($status['interned_strings_usage']['used_memory']),
                'strings_free_memory' => $this->size($status['interned_strings_usage']['free_memory']),
                'number_of_strings' => number_format($status['interned_strings_usage']['number_of_strings'])
            ];
        }


        $display = "";

        // memory
        $display .= sprintf("<div><h4>Memory used</h4> %s (%.2f%%) used of %s</div>",
            //($status['memory_usage']['used_memory'] + $status['memory_usage']['wasted_memory']),
            $info['readable']['used_memory'],
            $info['used_memory_percentage'],
            $info['readable']['total_memory']
        );
        $display .= "<br/>";

        // hit rate
        $display .= sprintf("<div><h4>Hit rate</h4> %.2f%%</div>",
            $info['hit_rate_percentage']
        );
        $display .= "<br/>";

        // keys
        $display .= sprintf("<div><h4>Used keys</h4> %.2f%%</div>",
            $info['used_key_percentage']
        );

        $display .= "<br/>";
//        $display .= "<br/>";

        // memory usage
        $display .= "<h4>Memory usage</h4>";
        $display .= sprintf("<b>total memory: </b> %s", $info['readable']['total_memory']);
        $display .= "<br/>";
        $display .= sprintf("<b>used memory:</b> %s", $info['readable']['used_memory']);
        $display .= "<br/>";
        $display .= sprintf("<b>free memory:</b> %s", $info['readable']['free_memory']);
        $display .= "<br/>";
        $display .= sprintf("<b>wasted memory:</b> %s", $info['wasted_percentage']);

        $display .= "<br/>";
//        $display .= "<br/>";

        // opcache stats
        $display .= "<h4>OPCache statistics</h4>";
        $display .= sprintf("<b>number of cached files:</b> %s", $info['readable']['num_cached_scripts']);
        $display .= "<br/>";
        $display .= sprintf("<b>number of hits:</b> %s", $info['readable']['hits']);
        $display .= "<br/>";
        $display .= sprintf("<b>number of misses:</b> %s", $info['readable']['misses']);
        $display .= "<br/>";
        $display .= sprintf("<b>blacklist misses:</b> %s", $info['readable']['blacklist_miss']);
        $display .= "<br/>";
        $display .= sprintf("<b>number of cached keys:</b> %s", $info['readable']['num_cached_keys']);
        $display .= "<br/>";
        $display .= sprintf("<b>max cached keys:</b> %s", $info['readable']['max_cached_keys']);

        $display .= "<br/>";
        $display .= "<br/>";

        // interned strings usage
        $display .= "<h4>Interned strings usage</h4>";
        $display .= sprintf("<b>buffer size:</b> %s", $info['readable']['interned']['buffer_size']);
        $display .= "<br/>";
        $display .= sprintf("<b>used memory:</b> %s", $info['readable']['interned']['strings_used_memory']);
        $display .= "<br/>";
        $display .= sprintf("<b>free memory:</b> %s", $info['readable']['interned']['strings_free_memory']);
        $display .= "<br/>";
        $display .= sprintf("<b>number of strings:</b> %s", $info['readable']['interned']['number_of_strings']);
        return $display;
    }

    /**
     * Format a size number into a string
     */
    private function size($size): string
    {
        // From: https://github.com/amnuts/opcache-gui/blob/master/index.php
        // Replaced $size / 1024 with $size  / 1000, and $size /= 1024 with $size /= 1000
        $i = 0;
        $val = ['b', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        while (($size / 1000) > 1) {
            $size /= 1000;
            ++$i;
        }
        return sprintf('%.2f%s%s', $size, '', $val[$i]);
    }

    /**
     * Get APCu display info
     */
    private function getAPCuCacheDisplayInfo()
    {
        $time = time();

        $status = apcu_cache_info(TRUE);
        $config = apcu_sma_info(TRUE);

        $used_memory = $status['mem_size'];
        $total_memory = $config['avail_mem'];

        $used_percent = 100. * ($used_memory / $total_memory);

        $display = "";

        // Memory usage
        $display .= sprintf("<div><h4>Memory used</h4> %s (%.2f%%) used of %s</div>",
            //($status['memory_usage']['used_memory'] + $status['memory_usage']['wasted_memory']),
            $this->size($used_memory),
            $used_percent,
            $this->size($total_memory)
        );
        $display .= "<br/>";

        // statistics
        $num_hits_and_misses = $status['num_hits'] + $status['num_misses'];
        $num_hits_and_misses = 0 >= $num_hits_and_misses ? 1 : $num_hits_and_misses;

        $number_vars = $status['num_entries'];
        $size_vars = $this->size($status['mem_size']);

        $req_rate_user = sprintf("%.2f", $status['num_hits'] ? (($status['num_hits'] + $status['num_misses']) / ($time - $status['start_time'])) : 0);
        $hit_rate_user = sprintf("%.2f", $status['num_hits'] ? (($status['num_hits']) / ($time - $status['start_time'])) : 0);
        $miss_rate_user = sprintf("%.2f", $status['num_misses'] ? (($status['num_misses']) / ($time - $status['start_time'])) : 0);
        $insert_rate_user = sprintf("%.2f", $status['num_inserts'] ? (($status['num_inserts']) / ($time - $status['start_time'])) : 0);

        $hits = sprintf("%s (%.2f%%)", $status['num_hits'], 100 * $status['num_hits'] / $num_hits_and_misses);
        $misses = sprintf("%s (%.2f%%)", $status['num_misses'], 100 * $status['num_misses'] / $num_hits_and_misses);
        $request_rate = sprintf("%s cache requests/second", $req_rate_user);
        $hit_rate = sprintf("%s cache requests/second", $hit_rate_user);
        $miss_rate = sprintf("%s cache requests/second", $miss_rate_user);
        $insert_rate = sprintf("%s cache requests/second", $insert_rate_user);
        $cache_full_count = sprintf("%s", $status['expunges']);

        $display .= "<h4>APCu statistics</h4>";
        $display .= sprintf("<b>Cached variables:</b> %s (%s)", $number_vars, $size_vars);
        $display .= "<br/>";
        $display .= sprintf("<b>Hits:</b> %s", $hits);
        $display .= "<br/>";
        $display .= sprintf("<b>Misses:</b> %s", $misses);
        $display .= "<br/>";
        $display .= sprintf("<b>Request rate (hits, misses):</b> %s", $request_rate);
        $display .= "<br/>";
        $display .= sprintf("<b>Hit rate:</b> %s", $hit_rate);
        $display .= "<br/>";
        $display .= sprintf("<b>Miss rate:</b> %s", $miss_rate);
        $display .= "<br/>";
        $display .= sprintf("<b>Insert rate:</b> %s", $insert_rate);
        $display .= "<br/>";
        $display .= sprintf("<b>Cache full count:</b> %s", $cache_full_count);

        return $display;
    }

    /**
     * Get Drupal cache display info
     */
    private function getDrupalCacheDisplayInfo()
    {
        $display = "";
        if (CU_Cachetags::doesCachetagsTableExist()) {
            $display .= sprintf("<b>Number of rows in <i>cachetags</i> table:</b> %d", CU_Cachetags::getNumRowsInCachetagsTable());
            $display .= "<br/>";
        }
        $display .= sprintf("<b>Number of rows in all <i>cache_*</i> tables:</b> %d", CU_DrupalCache::getNumRowsInAllCacheTables());
        $display .= "<br/>";
        return $display;
    }

    /**
     * {@inheritdoc}
     */
    public
    function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Retrieve the editable configuration.
        $config = $this->configFactory->getEditable('cache_utility.settings');
        $config->set("security.accessKey", $form_state->getValue(["settings", "security", "security_accessKey"]));
        $config->set("flushCaches.opcache", $form_state->getValue(["settings", "flush_caches", "opcache"]));
        $config->set("flushCaches.apcu", $form_state->getValue(["settings", "flush_caches", "apcu"]));
        $config->set("flushCaches.drupal_cachetags", $form_state->getValue(["settings", "flush_caches", "drupal_cachetags"]));
        $config->set("flushCaches.drupal_cachetables", $form_state->getValue(["settings", "flush_caches", "drupal_cachetables"]));

        $config->set('skip_ssl_verification', $form_state->getValue([
            'settings',
            'skip_ssl_verification',
        ]));

        $config->save();

        parent::submitForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
            'cache_utility.settings',
        ];
    }

    /**
     * This function parses a numeric string value from the php ini into a number.
     * Adapted from: The "return_bytes" example function shown in https://www.php.net/manual/en/function.ini-get.php
     */
    private function ini_parse_bytes($val)
    {
        $val = trim($val);
        $intval = intval($val);
        $last = strtolower($val[strlen($val) - 1]);
        switch ($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $intval *= 1024;
            case 'm':
                $intval *= 1024;
            case 'k':
                $intval *= 1024;
        }
        return $intval;
    }
}
